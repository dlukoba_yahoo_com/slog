package main

import (
	"time"
)

//Entry def here
type Entry struct {
	Data  Fields
	Time  time.Time
	Level Level
}
