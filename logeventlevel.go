package main

//Level doc here
type Level uint8

//this is a stringer
func (level Level) String() string {
	switch level {
	case DebugLevel:
		return "debug"
	case InfoLevel:
		return "info"
	case WarnLevel:
		return "warning"
	case ErrorLevel:
		return "error"
	case FatalLevel:
		return "fatal"
	case PanicLevel:
		return "panic"
	default:
		return "unknown"
	}
}

const (
	//DebugLevel doc here
	DebugLevel Level = iota //has value 0 which is default for uint8
	//InfoLevel doc here
	InfoLevel //1
	//WarnLevel doc here
	WarnLevel //2
	//ErrorLevel doc here
	ErrorLevel //3
	//FatalLevel doc here
	FatalLevel //4
	//PanicLevel doc here
	PanicLevel //5
)

//AllLevels doc here
var AllLevels = []Level{
	DebugLevel,
	InfoLevel,
	WarnLevel,
	ErrorLevel,
	FatalLevel,
	PanicLevel,
}
